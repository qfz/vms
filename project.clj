(defproject vms "0.2.9"
  :description "ArrowES VMS"
  :url "http://example.com/FIXME"
  :dependencies [[org.clojure/clojure "1.6.0"]
                 [org.clojure/clojurescript "0.0-2322"]
                 [org.clojure/core.async "0.1.338.0-5c5012-alpha"]
                 [ring "1.3.0"]
                 [compojure "1.1.8"]
                 [com.datomic/datomic-free "0.9.4815.12"]
                 [faker "0.2.2"]
                 [clj-http "0.9.2"]
                 [org.immutant/web "2.0.0-alpha1"]
                 [lib-noir "0.8.4"]
                 [com.cemerick/friend "0.2.1"]
                 [om "0.7.1"]
                 [cheshire "5.3.1"]]
  
  :plugins [[lein-ring "0.8.11"]
            [lein-cljsbuild "1.0.3"]]
  
  :source-paths ["src/clj"]

  :main vms.core

  :cljsbuild {:builds [{:id           "dev"
                        :source-paths ["src/cljs"]
                        :compiler     {:output-dir    "resources/public/js"
                                       :output-to     "resources/public/js/app.js"
                                       :optimizations :none
                                       :source-map    true}}]}
  
  :ring {:init    vms.core/init
         :handler vms.handler/app}
  
  :profiles {:uberjar {:aot :all}})
