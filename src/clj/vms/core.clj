(ns vms.core
  (:require [vms.handler :as handler])
  (:gen-class))


(defn -main [& args]
  (apply handler/-main args))
