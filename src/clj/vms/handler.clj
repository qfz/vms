(ns vms.handler
  (:require
   [ring.middleware.resource :refer [wrap-resource]]
   [ring.middleware.content-type :refer [wrap-content-type]]
   [ring.middleware.not-modified :refer [wrap-not-modified]]
   [ring.util.response :refer [response
                               resource-response
                               content-type
                               file-response
                               resource-response
                               redirect]]
   [compojure.core :as compojure :refer [defroutes
                                         ANY
                                         GET
                                         POST
                                         PUT
                                         context]]
   (compojure [handler :as handler]
              [route :as route])
   [vms.db :refer [conn
                   create-entity
                   retrieve-entity
                   update-entity
                   delete-entity
                   retrieve-entities
                   load-credential
                   entity-ids]]
   [clojure.java.io :as io :refer [resource]]
   [cemerick.friend :as friend]
   (cemerick.friend [workflows :as workflows]
                    [credentials :as credentials])
   [datomic.api :as d]
   [clj-http.client :as httpc]
   [immutant.web :as web]))


(defn dissoc-in
  "Dissociates an entry from a nested associative structure returning a new
  nested structure. keys is a sequence of keys. Any empty maps that result
  will not be present in the new structure."
  [m [k & ks :as keys]]
  (if ks
    (if-let [nextmap (get m k)]
      (let [newmap (dissoc-in nextmap ks)]
        (if (seq newmap)
          (assoc m k newmap)
          (dissoc m k)))
      m)
    (dissoc m k)))



(defn edn-response
  [data & [status]]
  {:status  (or status 200)
   :headers {"Content-Type" "application/edn"}
   :body    (pr-str data)})

(defn handle-signup
  [email password]
  ())



(defn datomic-request
  "Create a http request based on a given ring request map, for the
  consumption of datomic's REST API."
  [old-request port nested-url]
  (let [method (old-request :request-method)
        url (str "http://localhost:" port (subs (old-request :uri) (count nested-url)))
        body (old-request :body)]
    {:method method
     :url    url
     :headers {"Content-Type" "application/edn"}
     :params (old-request :params)
     :query-params (old-request :query-params)
     :body   body}))


(defroutes app-routes
  (GET "/" [] (-> (resource-response "public/index.html")
                  (content-type "text/html")
                  (friend/authenticated)))
  (GET "/signin" [] (resource "public/signin.html"))
  (GET "/signup" [] (resource "public/signup.html"))
  (POST "/signup" {{:keys [username password password-repeat]} :params} (create-entity
                                                                         {:user/email    username
                                                                          :user/passowrd (credentials/hash-bcrypt password)}))
  (GET "/signout" request (friend/logout* (redirect (str (:context request) "/signin"))))

  ;; These routes will only be excuted when the user is authenticated.
  (context "/api" []
    (context "/user" []
      (POST "/new" {attributes :body} (create-entity attributes))
      (GET "/ids" [] (edn-response (entity-ids :user/email)))
      (GET "/all" [] (edn-response (retrieve-entities :user/email)))
      (GET "/self" request (response (friend/current-authentication request)))
      (GET "/:id" [id] (retrieve-entity (read-string id)))
      (comment (PUT "/:id" [id :as {new-attributes :body}] (do (update-entity (read-string id)
                                                                              (edn/read-string (slurp new-attributes)))
                                                             {:status 200}))))
    (context "/vms" []
      (POST "/new" {attributes :body} (do (create-entity attributes)
                                        {:status 200}))
      (GET "/ids" [] (edn-response (entity-ids :vms/msg)))
      (GET "/:id" [id] (edn-response (retrieve-entity (read-string id))))
      (comment (PUT "/:id" [id :as {new-attributes :body}] (do (update-entity (read-string id)
                                                                              (edn/read-string (slurp new-attributes)))
                                                             {:status 200})))))
  (context "/datomic" []
    (ANY "/*" request (httpc/request (datomic-request request 8000 "/datomic"))))
  (route/resources "/")
  (route/not-found "Page Not Found4"))


(def app
  (-> app-routes
      (friend/authenticate {:credential-fn       (partial credentials/bcrypt-credential-fn load-credential)
                            :workflows           [(workflows/interactive-form)]
                            :login-uri           "/signin"
                            :default-landing-uri "/"})
      (handler/site)))

(defn -main [& {:as args}]
  (web/run app args))
