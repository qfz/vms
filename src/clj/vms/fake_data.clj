(ns vms.fake-data
  (:require [clojure.java.io :refer [resource]]
            [clojure.string :refer [join]]
            [datomic.api :as d]
            [faker.name :as name]
            [faker.internet :as internet]
            [faker.lorem :as lorem]
            [cemerick.friend.credentials :refer [hash-bcrypt]]
            [vms.db :refer [uri conn]])
  (:gen-class))


;; Parse schema file
(def schema-tx (read-string (slurp (resource "vms_schema.edn"))))

;; Submit schema transaction
(comment (d/transact conn schema-tx))


;; Sample transaction to add an attribute to VMS
(comment (d/transact conn
                     '[{:db/id                 #db/id[:db.part/db]
                        :db/ident              :vms/width
                        :db/valueType          :db.type/long
                        :db/cardinality        :db.cardinality/one
                        :db/fulltext           true
                        :db/doc                "Width of VMS in pixels"
                        :db.install/_attribute :db.part/db}]))

;; Sample transaction to delete an attribute
(comment (d/transact conn
                     [{:db/id     #db/id[db.part/user]
                       :db/excise :vms/height}]))


;;;;;;; A transaction to add fake users ;;;;;;;
(def user-roles [:user.roles/user
                 :user.roles/admin])

(defn make-tx-add-user
  "Return a map representing a user with all user's attributes"
  [_]
  {:db/id           (d/tempid :db.part/user)
   :user/first-name (name/first-name)
   :user/last-name  (name/last-name)
   :user/email      (internet/email)
   :user/password   (hash-bcrypt "abc123456")
   :user/role       user-roles})
;;;;;;; End ;;;;;;;

(defn make-tx-multi-adds
  [make-tx-add-entity n]
  (let [infinite-add-tx (iterate make-tx-add-entity (make-tx-add-entity 0))]
    (vec (take n infinite-add-tx))))


;;;;;;; Make transaction that adds fake VMS to d ;;;;;;;
(defn make-tx-add-vms
  [_]
  {:db/id   (d/tempid :db.part/user)
   :vms/msg (join " " (take (rand-int 10) (lorem/words)))})
;;;;;;; End ;;;;;;;



;;;;;;; Add references, to users and to VMS's ;;;;;;;
(defn all-vms-ids
  []
  (map
    first
    (d/q
      '[:find ?vms-id
        :where [?vms-id :vms/msg]]
      (d/db conn))))


(defn all-user-ids
  []
  (map
    first
    (d/q
      '[:find ?user-id
        :where [?user-id :user/email]]
      (d/db conn))))


(defn get-random-id
  [ids]
  (rand-nth (vec ids)))


(defn make-tx-insert-ref-to-targets
  [ref-ids target-ids ref-attribute]
  (map
    (fn
      [target-id]
      {:db/id        (d/tempid :db.part/user target-id)
       ref-attribute (d/tempid
                       :db.part/user
                       (get-random-id ref-ids))})
    target-ids))

(def msg (str (vec (repeat (* 48 28) "#000000"))))


;; Update vms/msg of all vms
(comment (for [id (all-vms-ids)]
           (d/transact conn
                       [{:db/id   id
                         :vms/msg msg}])))

(comment (for [id (all-vms-ids)]
           (d/transact conn
                       [{:db/id      id
                         :vms/height 28}])))

(comment (d/q '[:find ?name :where [_ :db.install/attribute ?a] [?a :db/ident ?name]]
              (d/db conn)))

(def colors (str ["#428bca" "#5cb85c" "#5bc0de" "#f0ad4e" "#d9534f"]))

(defn init-vms-colors
  [colors vms-ids]
  (for [id vms-ids]
    (d/transact conn [{:db/id      id
                       :vms/colors colors}])))

(defn init-vms-msg
  [msg vms-ids]
  (for [id vms-ids]
    (d/transact conn [{:db/id       id
                       :vms/message msg}])))

(defn init-db
  []
  (do
    ;; Create database
    (d/create-database uri)
    ;; Create schema
    (d/transact conn schema-tx)
    ;; Submit the "Add Users" transaction add-users-tx
    ;; Add 50 users to datomic
    (d/transact conn (make-tx-multi-adds make-tx-add-user 50))
    ;; Add 50 VMS's to datomic
    (d/transact conn (make-tx-multi-adds make-tx-add-vms 50))
    ;; Add VMS refs to users
    (d/transact
      conn
      (make-tx-insert-ref-to-targets
        (all-vms-ids)
        (all-user-ids)
        :user/vms))))
;;;;;;; End ;;;;;;;
