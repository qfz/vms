(ns vms.server
  (:require [clojure.java.io :as io]
            [clojure.edn :as edn]
            [clojure.pprint :refer [pprint]]
            [vms.db :refer [conn]]
            [datomic.api :as d]
            [cheshire.core :as cheshire])
  (:import (java.net Socket ServerSocket)
           (java.util.concurrent Executors)
           (com.fasterxml.jackson.core JsonParseException))
  (:gen-class))

(def client-sockets
  (atom
    {}))


;; Register the vms id and its associated socket in the client-sockets map.
(defn register
  [id socket]
  (swap! client-sockets assoc id socket))


(comment (defn handle
           [msg]
           @(d/transact
              conn
              [(assoc msg :vms/location (str (:vms/location msg)))])))

(defn handle
  "Handle a message (from client socket), and return a response."
  [client-socket msg]
  (try
    (let [edn-msg (cheshire/parse-string msg true)
          edn-response (merge edn-msg {:vms/updated? true})]
      (do
        (if (contains? edn-msg :db/id) (register (keyword (str (edn-msg :db/id))) client-socket))
        (str (cheshire/generate-string edn-response) \newline)))
    (catch JsonParseException json-exception
           (str (. json-exception getMessage) \newline))))


(defn send-to-client
  [client-socket msg]
  (let [writer (io/writer client-socket)]
    (. writer write msg)
    (. writer flush)))


(defn send-to-server
  [host port msg]
  (with-open [socket (new Socket host port)
              writer (io/writer socket)]
    (. writer write msg)
    (. writer flush)))


(defn listen
  "Listen to incoming message from client, handle the message then return a response."
  [client-socket]
  (let [in (io/reader client-socket)]
    (loop [input-line (. in readLine)]
      (when-not (nil? input-line)
        (send-to-client client-socket (handle client-socket input-line))
        (recur (. in readLine))))))


(comment (defn accept-connections
           "Accept TCP socket connections from clients. For each socket connection, spawn a thread
           to handle exchange of messages."
           [port]
           (let [server-socket (new ServerSocket port)]
             (while true
               (let [client-socket (. server-socket accept)]
                 (future (listen client-socket)))))))