(ns vms.db
  (:require [datomic.api :as d]
            [ring.util.response :refer [response]])
  (:gen-class))


(def uri "datomic:free://localhost:4334/vms-dev")

(def conn (d/connect uri))


(defn map->tx
  "Turn a map of attributes into a datomic transaction"
  ([attributes partition]
   (conj attributes
         {:db/id (d/tempid partition)}))
  ([attributes partition id]
   (conj attributes
         {:db/id (d/tempid partition id)})))


(defn create-entity
  [attributes]
  @(d/transact conn
               [(map->tx attributes :db.part/user)]))


(defn update-entity
  [id new-attributes]
  @(d/transact conn
               [(map->tx new-attributes :db.part/user id)]))


;; Todo
(defn delete-entity
  [context]
  ())


(defn retrieve-entity
  [id]
  (response (d/entity (d/db conn)
                      id)))


;; When querying for all IDs of a type of entity, Datomic return a set
;; of vectors. Each vector contains the ID of one entity. Datomic does
;; this because one could've queried for a number of attributes
;; instead of just the ID.  In that case, each vector within the
;; returned set contains all requested attributes of an entity.
;;
;; This function takes the returned set from Datomic, and turns the
;; inner vectors into just their first item.  This is intended to be
;; used when you want a set of all IDs instead of a set of Vectors of
;; ID.
(defn firsts
  [coll]
  (map (fn [a-coll]
         (first a-coll))
       coll))


;; Fix: use parameterized query instead of unquoting
(defn entity-ids
  [attribute]
  (firsts
    (d/q
      `[:find ?id
        :where [?id ~attribute]]
      (d/db conn))))

(defn ids->entity-maps
  [ids]
  (let [id->entity-map (fn [id]
                         (into {:db/id id} (d/entity (d/db conn) id)))]
    (map id->entity-map ids)))


;; datomic.Entity implements some interfaces of clojure map, but not all.
;; As a result, some collection functions such as update-in and assoc cannot be used
;; on datomic Entity.
;;
;; One solution is convert a datomic.Entity to a map, but when a datomic.Entity is
;; converted to a map, it loses its :db/id attribute (don't know why).
;;
;; So here I convert a datomic.Entity to string, and then construct a map from that string,
;; in order to preserve the :db/id attribute because I need it for the frontend.
;;
;; One of datomic's developers said on google group that the remaining map interfaces will be
;; implemented for datomic.Entity, but don't know when yet.
(comment (defn retrieve-entities
           "Get all entities that has the given attribute."
           [attribute]
           (response
             (vec (let [ids (entity-ids attribute)
                        entities (vec (ids->entity-maps ids))]
                    (if (contains? (get entities 0) :vms/msg)
                      (for [entity entities]
                        (let [temp-map (update-in (read-string (str entity)) [:vms/msg] read-string)]
                          (update-in temp-map [:vms/colors] read-string)))
                      entities))))))

(defn retrieve-entities
  [attribute]
  (vec (let [ids (entity-ids attribute)]
         (ids->entity-maps ids))))


(defn load-credential
  "Load credential from database. Because the db schema doesn't have username,
  user email is used as username."
  [email]
  (let [user (d/entity (d/db conn) [:user/email email])]
    {:username (:user/email user)
     :password (:user/password user)
     :roles    (:user/role user)}))
