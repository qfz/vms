(ns vms-client.component
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [cljs.core.async :refer [put! chan <! alts!]]
            [vms-client.canvas-drawing :as c]
            [goog.dom :as gdom]
            [goog.events :as events])
  (:import [goog.ui Toolbar]
           [goog.ui.editor DefaultToolbar ToolbarController]
           [goog.editor Command Field]
           [goog.editor.plugins BasicTextFormatter RemoveFormatting UndoRedo ListTabHandler SpacesTabHandler EnterHandler
            HeaderFormatter]
           [goog.editor.Field EventType]))

(defn get-index [target coll]
  (first (keep-indexed
           (fn [index item]
             (if (= target item)
               index))
           coll)))

(defn item-type
  [item]
  (condp = (count item)
    1 :vms
    2 :user))


(defn top-menu [cursor owner]
  (reify
    om/IInitState
    (init-state [this]
      {:selected-list :vms-list})
    om/IRenderState
    (render-state [this state]
      (dom/nav #js{:className   "top-bar"
                   :data-topbar ""}
        (dom/ul #js{:className "title-area"}
          (dom/li #js{:className "name"}
            (dom/h1 nil
              (dom/a #js{:href "#"}
                "ArrowES"))))
        (dom/section #js{:className "top-bar-section"}
          (dom/ul #js{:className "right show-for-large-up"}
            (dom/li #js{:className "divider"})
            (dom/li #js{:className ""} (dom/a {:href "#"} "Account"))
            (dom/li #js{:className "divider"})
            (dom/li #js{:className ""} (dom/a {:href "signout"} "Sign out")))
          (dom/ul #js{:className "left show-for-large-up"}
            (dom/li #js{:className "divider"})
            (dom/li #js{:className (str (if (= :vms-list (state :selected-list)) "active"))}
              (dom/a #js{:href    "#"
                         :onClick (fn [e]
                                    (put! (state :selected-list-ch) :vms-list)
                                    (om/set-state! owner :selected-list :vms-list))}
                "VMS"))
            (dom/li #js{:className "divider"})
            (dom/li #js{:className (str (if (= :user-list (state :selected-list)) "active"))}
              (dom/a #js{:href    "#"
                         :onClick (fn [event]
                                    (put! (state :selected-list-ch) :user-list)
                                    (om/set-state! owner :selected-list :user-list))}
                "Users"))
            (dom/li #js{:className "divider"})))))))


(defn left-menu [cursor owner]
  (reify
    om/IInitState
    (init-state [this]
      {:selected-item 0})
    om/IRenderState
    (render-state [this state]
      (dom/div #js{:style #js{:background-color "#fafafa"}}
        (apply dom/ul #js{:className "side-nav"}
          (for [item cursor]
            (let [idx (get-index item cursor)]
              (dom/li #js{:className (if (= (state :selected-item)
                                           idx)
                                       "active")}
                (dom/a #js{:href    "#"
                           :onClick (fn [e]
                                      (om/set-state! owner :selected-item idx)
                                      (put! (state :eid-ch) (@item 0)))}
                  (condp = (item-type item)
                    :vms (str "VMS: " (item 0))
                    :user (str "User: " (item 1))))))))))))


(defn color-picker [cursor owner]
  (reify
    om/IInitState
    (init-state [this]
      {:selected-color (cursor 0)})
    om/IRenderState
    (render-state [this state]
      (dom/div #js{:className "row"}
        (apply dom/div #js{:className "medium-12 columns"}
          (for [color cursor]
            (dom/div #js{:className (str "color-swatch" \space
                                      (if (= color (state :selected-color)) "color-picker-border"))
                         :style     #js{:background-color color}
                         :onClick   #(om/set-state! owner :selected-color color)})))))))


(defn tool-picker [cursor owner]
  (reify
    om/IInitState
    (init-state [this]
      {:selected-tool :pixel})
    om/IRenderState
    (render-state [this state]
      (dom/div
        #js{:className "row"}
        (dom/div
          #js{:className "medium-12 columns"}
          (dom/ul
            #js{:className "button-group radius"}
            (dom/li
              nil
              (dom/a #js{:className (str "tiny radius button secondary " (if (= :5x7-text (state :selected-tool)) "success"))
                         :onClick   #(om/set-state! owner :selected-tool :5x7-text)}
                "5x7 Text"))
            (dom/li
              nil
              (dom/a #js{:className (str "tiny radius button secondary " (if (= :pixel (state :selected-tool)) "success"))
                         :onClick   #(om/set-state! owner :selected-tool :pixel)}
                "Pixel"))
            (dom/li
              nil
              (dom/a #js{:className (str "tiny radius button secondary " (if (= :line (state :selected-tool)) "success"))
                         :onClick   #(om/set-state! owner :selected-tool :line)}
                "Line"))
            (dom/li
              nil
              (dom/a #js{:className (str "tiny radius button secondary " (if (= :rectangle (state :selected-tool)) "success"))
                         :onClick   #(om/set-state! owner :selected-tool :rectangle)}
                "Rectangle"))
            (dom/li
              nil
              (dom/a #js{:className (str "tiny radius button secondary " (if (= :circle (state :selected-tool)) "success"))
                         :onClick   #(om/set-state! owner :selected-tool :circle)}
                "Circle"))
            (dom/li
              nil
              (dom/a #js{:className (str "tiny radius button secondary " (if (= :ellipse (state :selected-tool)) "success"))
                         :onClick   #(om/set-state! owner :selected-tool :ellipse)}
                "Ellipse"))))))))

(def *gmap* nil)

(defn map-view [cursor owner]
  (reify
    om/IDidMount
    (did-mount [this]
      (let [opts #js{:center (js/google.maps.LatLng. -34.397 150.644)
                     :zoom   8}
            map-elem (js/google.maps.Map.
                       (.getElementById js/document "map-view")
                       opts)]
        (set! *gmap* map-elem)))
    om/IRender
    (render [this]
      (dom/div #js{:id    "map-view"
                   :style #js{:width  "500px"
                              :height "500px"}}
        "map-view"))))


(defn user-view [cursor owner]
  (reify
    om/IInitState
    (init-state [this]
      {:firstname       (cursor :user/first-name)
       :lastname        (cursor :user/last-name)
       :email           (cursor :user/email)
       :password        nil
       :password-repeat nil
       :roles           (cursor :user/roles)
       :success-alert   "hidden"
       :failure-alert   "hidden"
       :admin?          (contains? (cursor :user/role) :user.roles/admin)})
    om/IWillReceiveProps
    (will-receive-props [this new-cursor]
      (do
        (om/set-state! owner :firstname (new-cursor :user/first-name))
        (om/set-state! owner :lastname (new-cursor :user/last-name))
        (om/set-state! owner :email (new-cursor :user/email))
        (om/set-state! owner :roles (new-cursor :user/roles))
        (om/set-state! owner :admin? (contains? (new-cursor :user/role) :user.roles/admin))))
    om/IRenderState
    (render-state [this state]
      (dom/div  #js{:className "row"}
        (dom/div #js{:className "medium-6 columns"}
          (dom/div #js{:className ""
                       :style     #js{:margin-top "1rem"
                                      :float      "right"}}
            (dom/a #js{:className "tiny radius success button"}
              "New User"))
          (dom/form #js{}
            (dom/div #js{:className "row"}
              (dom/div #js{:className "medium-12 columns"}
                (dom/div #js{:className "row collapse prefix-radius"}
                  (dom/div #js{:className "small-3 columns"}
                    (dom/span #js{:className "prefix"} "First Name"))
                  (dom/div #js{:className "small-9 columns"}
                    (dom/input #js{:type        "text"
                                   :placeholder (state :firstname)})))
                (dom/div #js{:className "row collapse prefix-radius"}
                  (dom/div #js{:className "small-3 columns"}
                    (dom/span #js{:className "prefix"} "Last Name"))
                  (dom/div #js{:className "small-9 columns"}
                    (dom/input #js{:type        "text"
                                   :placeholder (state :lastname)})))
                (dom/div #js{:className "row collapse prefix-radius"}
                  (dom/div #js{:className "small-3 columns"}
                    (dom/span #js{:className "prefix"} "Email"))
                  (dom/div #js{:className "small-9 columns"}
                    (dom/input #js{:type        "text"
                                   :placeholder (state :email)})))
                (dom/div #js{:className "row collapse prefix-radius"}
                  (dom/div #js{:className "small-3 columns"}
                    (dom/span #js{:className "prefix"} "Password"))
                  (dom/div #js{:className "small-9 columns"}
                    (dom/input #js{:type        "text"
                                   :placeholder (state :password)})))
                (dom/div #js{:className "row collapse prefix-radius"}
                  (dom/div #js{:className "small-3 columns"}
                    (dom/span #js{:className "prefix"} "Repeat Password"))
                  (dom/div #js{:className "small-9 columns"}
                    (dom/input #js{:type        "text"
                                   :placeholder (state :password-repeat)})))
                (dom/div #js{:className "row collapse prefix-radius"}
                  (dom/div #js{:className "small-3 columns"}
                    (dom/span #js{:className "prefix"
                                  :style     #js{:border-right "1px solid #CCC"}}
                      "Admin?"))
                  (dom/div #js{:className "small-8 small-offset-1 columns switch"}
                    (dom/input #js{:id      "is-admin"
                                   :type    "checkbox"
                                   :checked (state :admin?)
                                   :onClick #(om/set-state! owner :admin? (not (state :admin?)))})
                    (dom/label #js{:htmlFor "is-admin"
                                   :style   #js{:margin-bottom "1rem"}})))
                (dom/div #js{:className "row collapse prefix-radius"}
                  (dom/div #js{:className "small-3 columns"}
                    (dom/span #js{:className "prefix"
                                  :style     #js{:border-right "1px solid #CCC"}}
                      "VMS"))
                  (dom/div #js{:className "small-9 columns"}
                    (dom/ul #js{:className ""
                                :style     #js{:list-style-type "none"}}
                      (dom/li #js{:className ""} "Some VMS ID"))))
                (dom/div #js{:className "row collapse"}
                  (dom/a #js{:href      "#"
                             :className "tiny radius button"
                             :style     #js{:margin-right "1rem"}}
                    "Save")
                  (dom/a #js{:href      "#"
                             :className "tiny radius secondary button"}
                    "Cancel"))))))))))

(defn vms-view [cursor owner]
  (reify
    om/IRenderState
    (render-state [this state]
      (dom/div #js{:className "row"}
        (dom/div #js{:className "medium-2 columns"}
          "frames")
        (dom/div #js{:id        "canvas-container"
                     :className "medium-8 columns"
                     :ref       "canvas-container-ref"}
          (dom/div #js{:className "row"
                       :style     #js{:margin-top "0.5rem"}}
            (dom/div #js{:className "medium-12 columns"}
              (dom/a #js{:className "tiny radius success button"
                         :style     #js{:float "right"}}
                "New VMS")))
          (om/build main-canvas (cursor :vms/msg)))
        (dom/div #js{:className "medium-2 columns"}
          "tools")))))


(defn main-canvas [cursor owner]
  (reify
    om/IInitState
    (init-state [this]
      {:zoom-factor 8})
    om/IDidMount
    (did-mount [this]
      (let [main-canvas-elem (om/get-node owner)
            zoom-factor (om/get-state owner :zoom-factor)
            canvas-container-width (.-offsetWidth (.getElementById js/document "canvas-container"))
            h-cell-count (count (cursor 0))
            zoom-factor (/ (- canvas-container-width 30) h-cell-count)]
        (c/draw-image cursor main-canvas-elem zoom-factor "#000000")))
    om/IRenderState
    (render-state [this state]
      (let [width (count (cursor 0))
            height (count cursor)
            zoom-factor (state :zoom-factor)]
        (dom/canvas #js{:id        "main-canvas"
                        :className ""
                        :width     width
                        :height    height
                        :ref       "main-canvas-elem"})))))


(defn working-canvas [cursor owner]
  (reify
    om/IRender
    (render [this]
      (dom/canvs #js{:ref   "active-canvas-ref"
                     :style #js{:position       "absolute"
                                :top            "0"
                                :left           "0"
                                :pointer-events "none"}}))))


(defn working-canvas-wrapper [cursor owner]
  (reify
    om/IRenderState
    (render-state [this state]
      (let [active-canvas (om/get-node owner "active-canvas-ref")]
        (dom/div #js{})))))


(def toolbar nil)
(def content-field nil)
(def toolbar-controller nil)

;; Make Uint8ClampedArray Seqable, necessary for using coll functions on Uint8ClampedArray
(extend-type js/Uint8ClampedArray
  ISeqable
  (-seq [array] (array-seq array 0)))

(defn four-vals->rbga
  "Turns a list of 4 values (red, blue, green, alpha) into a rbga string"
  [four-vals]
  (str "rbga(" (nth four-vals 0) "," (nth four-vals 1) "," (nth four-vals 2) "," (nth four-vals 3) ")"))


(defn content->pixels
  "Content is some HTML in String"
  [content width height]
  (let [canvas (.getElementById js/document "text-layer")
        context (.getContext canvas "2d")]
    (set! (.-width canvas) width)
    (set! (.-height canvas) height)
    (set! (.-fillStyle context) "red")
    (.drawHTML js/rasterizeHTML content canvas)
    (let [image-data (.. context (getImageData 0 0 width height) -data)]
      (map four-vals->rbga (partition 4 image-data)))))

(defn render-text [e]
  (let [content (.getCleanContents content-field)
        pixels (content->pixels (str "<p>" content "</p>") 96 56)]
    (println pixels)))



(comment (defn text-editor
  "Text Editor using Google Closure's default toolbar"
  [cursor owner]
  (reify
    om/IDidMount
    (did-mount [this]
      (let [toolbar-div (.getElementById js/document "toolbar")
            buttons #js[Command/UNDO Command/REDO Command/REMOVE_FORMAT Command/BOLD Command/ITALIC
                        Command/STRIKE_THROUGH Command/UNDERLINE Command/FONT_COLOR
                        Command/BACKGROUND_COLOR Command/FONT_FACE Command/FONT_SIZE
                        Command/JUSTIFY_LEFT Command/JUSTIFY_CENTER Command/JUSTIFY_RIGHT]]
        (set! toolbar (.makeToolbar DefaultToolbar buttons toolbar-div))
        (set! content-field (Field. "content-field"))
        ;; Register plugins to the content field
        (.registerPlugin content-field (new BasicTextFormatter))
        (.registerPlugin content-field (new RemoveFormatting))
        (.registerPlugin content-field (new UndoRedo))
        (.registerPlugin content-field (new ListTabHandler))
        (.registerPlugin content-field (new SpacesTabHandler))
        (.registerPlugin content-field (new EnterHandler))
        (.registerPlugin content-field (new HeaderFormatter))
        ;; Hook the toolbar into the content field
        (set! toolbar-controller (ToolbarController. content-field toolbar))
        ;; Make the content field editable
        (.makeEditable content-field)
        (events/listen content-field EventType/DELAYEDCHANGE render-text)))
    om/IRenderState
    (render-state [this state]
      (dom/div nil
        (dom/div #js{:id        "toolbar"
                     :className "goog-toolbar"})
        (dom/div #js{:id    "content-field"
                     :style #js{:border "1px solid grey"
                                :width  "100%"}}))))))



;; Text-editor component for TinyMCE
(comment (defn text-editor [cursor owner]
           (reify
             om/IDidMount
             (did-mount [this]
               (. js/tinymce (init #js{:selector  "textarea"
                                       :menubar   false
                                       :statusbar false
                                       :plugins   "textcolor"
                                       :toolbar   "undo redo | removeformat | fontselect | fontsizeselect | forecolor backcolor | bold italic underline strikethrough | alignleft aligncenter alignright"
                                       :setup (fn [editor]
                                                (.on editor "change" (fn [e]
                                                                       (println (.. js/tinymce -activeEditor (getContent))))))})))
             om/IRender
             (render [this]
               (dom/textarea #js{})))))
