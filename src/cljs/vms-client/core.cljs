(ns vms-client.core
  (:require-macros [cljs.core.async.macros :refer [go]])
  (:require [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [cljs.core.async :refer [put! chan <! alts!]]
            [clojure.string :as string]
            [cljs.reader :as reader]
            [goog.events :as events]
            [goog.string :as gstring]
            [vms-client.font-5x7 :as font-5x7]
            [vms-client.state :refer [app-state]]
            [vms-client.component :as c]
            [vms-client.util :refer [edn-xhr]])
  (:import [goog.net XhrIo]
           [goog.events EventType KeyHandler KeyCodes]))


(enable-console-print!)


(defn index-of [id coll]
  (first (keep-indexed
           (fn [index item]
             (if (= id (item :db/id))
               index))
           coll)))

(defn get-index [target coll]
  (first (keep-indexed
           (fn [index item]
             (if (= target item)
               index))
           coll)))


;; Functions to call on window re-size.
(def resize-fns
  (atom []))

;; Function to be assigned to window.onresize. It calls all the
;; functions in resize-fns.
(defn resize-all
  []
  (doseq [f @resize-fns]
    (f)))

(set! (. js/window -onresize) resize-all)


(defn item-type
  [item]
  (condp = (count item)
    1 :vms
    2 :user))



;; did-mount for msg-canvas
(comment (did-mount [this]
           (let [paper (js/Raphael "msg-canvas" 200 200)
                 resize (fn []
                          (let [canvas-width (.. js/document
                                               (getElementById "msg-canvas")
                                               -offsetWidth)]
                            (. paper (setSize canvas-width canvas-width))))
                 vms-index (index-of (cursor :selected-entity) (cursor :vms-list))
                 row-count (count (get-in cursor [:vms-list vms-index :vms/msg]))
                 column-count (count (get-in cursor [:vms-list vms-index :vms/msg 0]))]
             (dotimes [x row-count]
               (dotimes [y column-count]
                 (.. paper
                   (rect (* x 10) (* y 10) 10 10)
                   (attr "stroke" "#999999")
                   (attr "stroke-width" 3)
                   (attr "fill" (get-in cursor [:vms-list vms-index :vms/msg x y])))))
             (. paper (setViewBox 0 0 200 200))
             (swap! resize-fns (fn [fns] (conj fns resize)))
             (resize))))


;;------- Message Editor & Co. -------

;; For the 5x7-text tool, I want to display a vertical bar to indicate the
;; edit position of text. To do this, I originally intended to make the left
;; border of the 7 pixels below the edit position to be yellow. However, html table gives
;; precedence to right border over left border. That is, the style of the right border of
;; a pixel will override the style of the left border of the next pixel on the right.
;;
;; So I instead look for the 7 pixels below the edit position but one step to the left,
;; and change the style of their right borders to indicate the edit position.
(comment (defcomponent msg-canvas [cursor owner]
           (init-state [this]
             (let [vms-index (index-of (cursor :selected-entity) (cursor :vms-list))]
               {:msg                (get-in cursor [:vms-list vms-index :vms/msg])
                :drawing-rectangle? false
                :drawing-line?      false
                :drawing-circle?    false}))
           (render-state [this local-state]
             (let [vms-index (index-of (cursor :selected-entity) (cursor :vms-list))
                   msg (get-in cursor [:vms-list vms-index :vms/msg])
                   row-count (count (get-in cursor [:vms-list vms-index :vms/msg]))
                   column-count (count (get-in cursor [:vms-list vms-index :vms/msg 0]))
                   draw-rectangle (fn []
                                    (let [start-row (get-in @cursor [:rectangle :start-point 0])
                                          start-column (get-in @cursor [:rectangle :start-point 1])
                                          end-row (get-in @cursor [:rectangle :end-point 0])
                                          end-column (get-in @cursor [:rectangle :end-point 1])
                                          width (+ (Math/abs (- start-column end-column)) 1)
                                          height (+ (Math/abs (- start-row end-row)) 1)
                                          color (@cursor :selected-color)]
                                      (cond
                                        (and (< start-row end-row) (< start-column end-column)) (do
                                                                                                  (dotimes [h height]
                                                                                                    (om/update! cursor [:vms-list vms-index :vms/msg (+ start-row h) start-column] color)
                                                                                                    (om/update! cursor [:vms-list vms-index :vms/msg (+ start-row h) end-column] color))
                                                                                                  (dotimes [w width]
                                                                                                    (om/update! cursor [:vms-list vms-index :vms/msg start-row (+ start-column w)] color)
                                                                                                    (om/update! cursor [:vms-list vms-index :vms/msg end-row (+ start-column w)] color)))
                                        :else nil)))
                   draw-line (fn []
                               (let [start-row (get-in @cursor [:line :start-point 0])
                                     start-column (get-in @cursor [:line :start-point 1])
                                     end-row (get-in @cursor [:line :end-point 0])
                                     end-column (get-in @cursor [:line :end-point 1])
                                     dx (+ (Math/abs (- end-column start-column)) 1)
                                     dy (+ (Math/abs (- end-row start-row)) 1)
                                     color (@cursor :selected-color)]
                                 (cond
                                   (and (< start-column end-column) (< start-row end-row))
                                   (loop [x start-column
                                          y start-row
                                          eps 0]
                                     (if (<= x end-column)
                                       (do
                                         (om/update! cursor [:vms-list vms-index :vms/msg y x] color)
                                         (recur
                                           (+ x 1)
                                           (if (< (* 2 (+ eps dy)) dx)
                                             y
                                             (+ y 1))
                                           (if (< (* 2 (+ eps dy)) dx)
                                             (+ eps dy)
                                             (- (+ eps dy) dx))))))
                                   (and (< start-column end-column) (> start-row end-row))
                                   (loop [x start-column
                                          y start-row
                                          eps 0]
                                     (if (<= x end-column)
                                       (do
                                         (om/update! cursor [:vms-list vms-index :vms/msg y x] color)
                                         (recur
                                           (+ x 1)
                                           (if (< (* 2 (+ eps dy)) dx)
                                             y
                                             (- y 1))
                                           (if (< (* 2 (+ eps dy)) dx)
                                             (+ eps dy)
                                             (- (+ eps dy) dx)))))))))
                   draw-circle (fn []
                                 (let [x0 (get-in @cursor [:circle :center-point 1])
                                       y0 (get-in @cursor [:circle :center-point 0])
                                       x1 (get-in @cursor [:circle :edge-point 1])
                                       y1 (get-in @cursor [:circle :edge-point 0])
                                       radius (Math/round (Math/sqrt (+ (Math/pow (- x1 x0) 2) (Math/pow (- y1 y0) 2))))
                                       color (@cursor :selected-color)]
                                   (loop [x radius
                                          y 0
                                          radius-error (- 1 radius)]
                                     (if (>= x y)
                                       (do
                                         (om/update! cursor [:vms-list vms-index :vms/msg (+ y y0) (+ x x0)] color)
                                         (om/update! cursor [:vms-list vms-index :vms/msg (+ x y0) (+ y x0)] color)
                                         (om/update! cursor [:vms-list vms-index :vms/msg (+ y y0) (- x0 x)] color)
                                         (om/update! cursor [:vms-list vms-index :vms/msg (+ x y0) (- x0 y)] color)
                                         (om/update! cursor [:vms-list vms-index :vms/msg (- y0 y) (- x0 x)] color)
                                         (om/update! cursor [:vms-list vms-index :vms/msg (- y0 x) (- x0 y)] color)
                                         (om/update! cursor [:vms-list vms-index :vms/msg (- y0 y) (+ x x0)] color)
                                         (om/update! cursor [:vms-list vms-index :vms/msg (- y0 x) (+ y x0)] color)
                                         (recur
                                           (if (< radius-error 0)
                                             x
                                             (- x 1))
                                           (+ y 1)
                                           (if (< radius-error 0)
                                             (+ radius-error (+ 1 (* 2 y)))
                                             (+ radius-error (* 2 (+ 1 (- y x)))))))))))
                   print-points (fn []
                                  (println (@cursor :rectangle)))]
               (dom/div {:class "row"}
                 (dom/table {:width       "100%"
                             :empty-cells "show"
                             :class       "unselectable"
                             :style       {:border-collapse "collapse"}}
                   (map-indexed
                     (fn [x row]
                       (dom/tr {:class "unselectable"}
                         (map-indexed
                           (fn [y column]
                             (let [handle-pixel-click (fn [click-event]
                                                        (if-not (= (@cursor :selected-color) (get-in @cursor [:vms-list vms-index :vms/msg x y]))
                                                          (om/update! cursor [:vms-list vms-index :vms/msg x y]
                                                            (@cursor :selected-color))
                                                          (om/update! cursor [:vms-list vms-index :vms/msg x y] "#000000")))
                                   handle-5x7-click (fn [click-event]
                                                      (if (and (>= (- row-count x) 7)
                                                            (< y column-count))
                                                        (om/update! cursor :edit-position [x y])))
                                   handle-rectangle-click (fn [e]
                                                            (if (local-state :drawing-rectangle?)
                                                              (do
                                                                (om/set-state! owner :drawing-rectangle? false)
                                                                (om/update! cursor [:rectangle :end-point] [x y])
                                                                (print-points)
                                                                (draw-rectangle))
                                                              (do
                                                                (om/set-state! owner :drawing-rectangle? true)
                                                                (om/update! cursor [:rectangle :start-point] [x y])
                                                                (om/update! cursor [:rectangle :end-point] [nil nil])
                                                                (om/update! cursor [:vms-list vms-index :vms/msg x y] (@cursor :selected-color)))))
                                   handle-line-click (fn [e]
                                                       (if (local-state :drawing-line?)
                                                         (do
                                                           (om/set-state! owner :drawing-line? false)
                                                           (om/update! cursor [:line :end-point] [x y])
                                                           (draw-line))
                                                         (do
                                                           (om/set-state! owner :drawing-line? true)
                                                           (om/update! cursor [:line :start-point] [x y])
                                                           (om/update! cursor [:line :end-point] [nil nil])
                                                           (om/update! cursor [:vms-list vms-index :vms/msg x y] (@cursor :selected-color)))))
                                   handle-circle-click (fn [e]
                                                         (if (local-state :drawing-circle?)
                                                           (do
                                                             (om/set-state! owner :drawing-circle? false)
                                                             (om/update! cursor [:circle :edge-point] [x y])
                                                             (draw-circle))
                                                           (do
                                                             (om/set-state! owner :drawing-circle? true)
                                                             (om/update! cursor [:circle :center-point] [x y])
                                                             (om/update! cursor [:circle :edge-point] [nil nil])
                                                             (om/update! cursor [:vms-list vms-index :vms/msg x y] (@cursor :selected-color)))))
                                   make-click-handler (fn [drawing-tool]
                                                        (condp = drawing-tool
                                                          :pixel handle-pixel-click
                                                          :5x7-text handle-5x7-click
                                                          :rectangle handle-rectangle-click
                                                          :line handle-line-click
                                                          :circle handle-circle-click
                                                          nil))]
                               (dom/td {:style    {:text-align       "center"
                                                   :background-color (get-in cursor [:vms-list vms-index :vms/msg x y])
                                                   :border-top       "2px solid #999999"
                                                   :border-right     (if (and (= (cursor :selected-tool) :5x7-text)
                                                                           (not (empty? (cursor :edit-position)))
                                                                           (<= (get-in cursor [:edit-position 0]) x (+ (get-in cursor [:edit-position 0]) 6))
                                                                           (= y (- (get-in cursor [:edit-position 1]) 1)))
                                                                       "2px solid #fec506"
                                                                       "2px solid #999999")
                                                   :border-bottom    "2px solid #999999"
                                                   :border-left      (if (and (= y 0)
                                                                           (<= (get-in cursor [:edit-position 0]) x (+ (get-in cursor [:edit-position 0]) 6))
                                                                           (= y (get-in cursor [:edit-position 1])))
                                                                       "2px solid #fec506"
                                                                       "2px solid #999999")
                                                   :cursor           "default"
                                                   :height           (str (/ 58 column-count) "vw")}
                                        :on-click (make-click-handler (cursor :selected-tool))
                                        :class    "unselectable"})))
                           row)))
                     msg)))))))

(defn msg-canvas [cursor owner]
  (reify
    om/IRenderState
    (render-state [this state]
      (let [row-count (count cursor)
            column-count (count (cursor 0))]
        (dom/div #js{:className "row"}
          (apply dom/table #js{:width       "100%"
                               :empty-cells "show"
                               :className   "unselectable"
                               :style       #js{:border-collapse "collapse"}}
            (map-indexed
              (fn [x row]
                (apply dom/tr #js{}
                  (map-indexed
                    (fn [y column]
                      (dom/td #js{:className "unselectable"
                                  :style     #js{:text-align       "center"
                                                 :background-color (get-in cursor [x y])
                                                 :cursor           "default"
                                                 :height           (str (/ 58 column-count) "vw")
                                                 :border           "2px solid #999999"}}))
                    row)))
              cursor)))))))



(comment (defcomponent map-view [location owner]
           (did-mount [this]
             (let [loc-map (.. js/L
                             (map "map-view")
                             (setView #js[51.505, -0.09] 13))]
               (set! (.. js/L -Icon -Default -imagePath) "lib/leaflet/images")
               (.. js/L
                 (tileLayer "http://{s}.tile.osm.org/{z}/{x}/{y}.png")
                 (addTo loc-map))
               (.. js/L
                 (marker #js[51.5, -0.09])
                 (addTo loc-map))))
           (render [this]
             (dom/div {:id "map-view"}
               "map-view"))))


(defn entity-type
  [entity]
  (cond
    (contains? entity :vms/msg) :vms
    (contains? entity :user/email) :user))


(defn update-vals
  "Update multiple value of a map"
  [map vals f]
  (reduce #(update-in % [%2] f) map vals))

(defn init-state []
  (do
    (edn-xhr {:method      :post
              :url         "datomic/api/query"
              :data        (quote {:q    [:find ?id ?lastname
                                          :in $
                                          :where [?id :user/last-name ?lastname]]
                                   :args [{:db/alias "free/vms-dev"}]})
              :on-complete (fn [data]
                             (do
                               (om/update! cursor [:user-list] data)))})
    (edn-xhr {:method      :post
              :url         "datomic/api/query"
              :data        (quote {:q    [:find ?id
                                          :in $
                                          :where [?id :vms/msg]]
                                   :args [{:db/alias "free/vms-dev"}]})
              :on-complete (fn [data]
                             (do
                               (om/update! cursor [:vms-list] data)))})))


(defn app [cursor owner]
  (reify
    om/IInitState
    (init-state [this]
      {:selected-list-ch (chan)
       :eid-ch           (chan)
       :selected-list    :vms-list
       :entity-ch        (chan)})
    om/IWillMount
    (will-mount [this]
      (do
        (let [selected-list-ch (om/get-state owner :selected-list-ch)]
          (go (while true
                (let [selected-list (<! selected-list-ch)]
                  (om/set-state! owner :selected-list selected-list)))))
        (let [eid-ch (om/get-state owner :eid-ch)]
          (go
            (while true
              (let [eid (<! eid-ch)]
                (comment (edn-xhr {:method      :get
                                   :url         (str "datomic/data/free/vms-dev/-/entity?e=" eid)
                                   :on-complete (fn [data]
                                                  (let [entity (if (contains? data :vms/msg)
                                                                 (update-vals data [:vms/msg :vms/colors] reader/read-string)
                                                                 data)]
                                                    (om/update! cursor :entity entity)
                                                    (println (@cursor :entity))))}))))))))
    om/IRenderState
    (render-state [this state]
      (dom/div nil
        (om/build c/top-menu {} {:init-state (select-keys state [:selected-list-ch])})
        (dom/div #js{:className "row"}
          (dom/div #js{:className "medium-2 columns"}
            (om/build c/left-menu
              (condp = (state :selected-list)
                :vms-list (cursor :vms-list)
                :user-list (cursor :user-list))
              {:init-state (select-keys state [:eid-ch])}))
          (dom/div #js{:className "medium-10 columns"}
            (condp = (entity-type (cursor :entity))
              :vms (om/build c/vms-view (cursor :entity))
              :user (om/build c/user-view (cursor :entity))
              (om/build c/map-view (cursor :entity)))))))))


(om/root app
  app-state
  {:target (. js/document (getElementById "app"))})


;; character is a vector of vectors that represents a dot matrix font character.
(defn draw-char [character height width]
  (let [idx (index-of (@app-state :selected-entity) (@app-state :vms-list))
        msg (get-in @app-state [:vms-list idx :vms/msg])
        row-count (count msg)
        column-count (count (msg 0))
        pos-x (get-in @app-state [:edit-position 0])
        pos-y (get-in @app-state [:edit-position 1])
        color (@app-state :selected-color)]
    (do
      (if (>= (- column-count pos-y) 11)
        (swap! app-state
          update-in [:edit-position 1] (fn [old-val]
                                         (+ old-val 6))))
      (if (>= (- column-count pos-y) width)
        (dotimes [x height]
          (dotimes [y width]
            (swap! app-state
              update-in [:vms-list idx :vms/msg (+ pos-x x) (+ pos-y y)] (fn [old-val]
                                                                           (if (= 1 (get-in character [x y]))
                                                                             color
                                                                             "#000000")))))))))

;; Only deletes one column.
(defn delete-char [height]
  (let [idx (index-of (@app-state :selected-entity) (@app-state :vms-list))
        pos-row (get-in @app-state [:edit-position 0])
        pos-column (get-in @app-state [:edit-position 1])]
    (if (>= pos-column 0)
      (do
        (dotimes [x height]
          (swap! app-state
            assoc-in [:vms-list idx :vms/msg (+ pos-row x) pos-column]
            "#000000"))
        (swap! app-state
          update-in [:edit-position 1] (fn [old-val]
                                         (if (>= (- pos-column 1) 0)
                                           (- pos-column 1)
                                           0)))))))


;; Space is one column wide.
(defn draw-space [height]
  (let [idx (index-of (@app-state :selected-entity) (@app-state :vms-list))
        pos-row (get-in @app-state [:edit-position 0])
        pos-column (get-in @app-state [:edit-position 1])
        msg (get-in @app-state [:vms-list idx :vms/msg])
        column-count (count (msg 0))]
    (do
      (if (< (+ pos-column 1) column-count)
        (swap! app-state
          assoc-in [:edit-position 1] (+ pos-column 1))))))

(def root-key-handler (new KeyHandler js/document))


(events/listen root-key-handler
  (.. KeyHandler -EventType -KEY)
  (fn [e]
    (if (= (@app-state :selected-tool) :5x7-text)
      (condp = (. e -keyCode)
        (. KeyCodes -A) (draw-char font-5x7/A 7 5)
        (. KeyCodes -B) (draw-char font-5x7/B 7 5)
        (. KeyCodes -C) (draw-char font-5x7/C 7 5)
        (. KeyCodes -D) (draw-char font-5x7/D 7 5)
        (. KeyCodes -E) (draw-char font-5x7/E 7 5)
        (. KeyCodes -F) (draw-char font-5x7/F 7 5)
        (. KeyCodes -G) (draw-char font-5x7/G 7 5)
        (. KeyCodes -H) (draw-char font-5x7/H 7 5)
        (. KeyCodes -I) (draw-char font-5x7/I 7 5)
        (. KeyCodes -J) (draw-char font-5x7/J 7 5)
        (. KeyCodes -K) (draw-char font-5x7/K 7 5)
        (. KeyCodes -L) (draw-char font-5x7/L 7 5)
        (. KeyCodes -M) (draw-char font-5x7/M 7 5)
        (. KeyCodes -N) (draw-char font-5x7/N 7 5)
        (. KeyCodes -O) (draw-char font-5x7/O 7 5)
        (. KeyCodes -P) (draw-char font-5x7/P 7 5)
        (. KeyCodes -Q) (draw-char font-5x7/Q 7 5)
        (. KeyCodes -R) (draw-char font-5x7/R 7 5)
        (. KeyCodes -S) (draw-char font-5x7/S 7 5)
        (. KeyCodes -T) (draw-char font-5x7/T 7 5)
        (. KeyCodes -U) (draw-char font-5x7/U 7 5)
        (. KeyCodes -V) (draw-char font-5x7/V 7 5)
        (. KeyCodes -W) (draw-char font-5x7/W 7 5)
        (. KeyCodes -X) (draw-char font-5x7/X 7 5)
        (. KeyCodes -Y) (draw-char font-5x7/Y 7 5)
        (. KeyCodes -Z) (draw-char font-5x7/Z 7 5)
        (. KeyCodes -ZERO) (draw-char font-5x7/zero 7 5)
        (. KeyCodes -ONE) (draw-char font-5x7/one 7 5)
        (. KeyCodes -TWO) (draw-char font-5x7/two 7 5)
        (. KeyCodes -THREE) (draw-char font-5x7/three 7 5)
        (. KeyCodes -FOUR) (draw-char font-5x7/four 7 5)
        (. KeyCodes -FIVE) (draw-char font-5x7/five 7 5)
        (. KeyCodes -SIX) (draw-char font-5x7/six 7 5)
        (. KeyCodes -SEVEN) (draw-char font-5x7/seven 7 5)
        (. KeyCodes -EIGHT) (draw-char font-5x7/eight 7 5)
        (. KeyCodes -NINE) (draw-char font-5x7/nine 7 5)
        (. KeyCodes -NUM_ZERO) (draw-char font-5x7/zero 7 5)
        (. KeyCodes -NUM_ONE) (draw-char font-5x7/one 7 5)
        (. KeyCodes -NUM_TWO) (draw-char font-5x7/two 7 5)
        (. KeyCodes -NUM_THREE) (draw-char font-5x7/three 7 5)
        (. KeyCodes -NUM_FOUR) (draw-char font-5x7/four 7 5)
        (. KeyCodes -NUM_FIVE) (draw-char font-5x7/five 7 5)
        (. KeyCodes -NUM_SIX) (draw-char font-5x7/six 7 5)
        (. KeyCodes -NUM_SEVEN) (draw-char font-5x7/seven 7 5)
        (. KeyCodes -NUM_EIGHT) (draw-char font-5x7/eight 7 5)
        (. KeyCodes -NUM_NINE) (draw-char font-5x7/nine 7 5)
        (. KeyCodes -DASH) (draw-char font-5x7/dash 7 5)
        (. KeyCodes -EQUALS) (draw-char font-5x7/equals 7 5)
        (. KeyCodes -OPEN_SQUARE_BRACKET) (draw-char font-5x7/open-bracket 7 5)
        (. KeyCodes -CLOSE_SQUARE_BRACKET) (draw-char font-5x7/close-bracket 7 5)
        (. KeyCodes -BACKSLASH) (draw-char font-5x7/backslash 7 5)
        (. KeyCodes -SEMICOLON) (draw-char font-5x7/semicolon 7 5)
        (. KeyCodes -SINGLE_QUOTE) (draw-char font-5x7/single-quote 7 5)
        (. KeyCodes -COMMA) (draw-char font-5x7/comma 7 5)
        (. KeyCodes -PERIOD) (draw-char font-5x7/period 7 5)
        (. KeyCodes -SLASH) (draw-char font-5x7/slash 7 5)
        (. KeyCodes -TILDE) (draw-char font-5x7/tilde 7 5)
        (. KeyCodes -BACKSPACE) (do
                                  (. e preventDefault)
                                  (delete-char 7))
        (. KeyCodes -SPACE) (do
                              (. e preventDefault)
                              (draw-space 7))))))
