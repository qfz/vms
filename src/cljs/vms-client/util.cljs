(ns vms-client.util
  (:require [goog.events :as events]
            [cljs.reader :as reader])
  (:import [goog.net XhrIo]
           [goog.events EventType KeyHandler KeyCodes]))

(def ^:private meths
  {:get    "GET"
   :put    "PUT"
   :post   "POST"
   :delete "DELETE"})

;; xhr means XMLHttpRequest
(defn edn-xhr [{:keys [method url data on-complete]}]
  (let [xhr (XhrIo.)]
    (events/listen xhr goog.net.EventType.COMPLETE
                   (fn [e]
                     (on-complete (reader/read-string (.getResponseText xhr)))))
    (. xhr
       (send url (meths method) (when data (pr-str data))
             #js {"Content-Type" "application/edn"}))))