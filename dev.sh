#!/bin/bash

cd ~/opt/datomic-free-0.9.4815

# Start datomic transactor
bin/transactor config/samples/free-transactor-template.properties &

# Start the REST API server of datomic
bin/rest -o "*" -p 9000 free datomic:free://localhost:4334/ &

cd ~/repos/vms

# Start ring server
lein ring server &

